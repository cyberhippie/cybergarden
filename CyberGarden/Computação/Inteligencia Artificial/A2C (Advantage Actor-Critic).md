---
tipo:
  - estudo
title: A2C (Advantage Actor-Critic)
create: 2023-11-07 00:56
descricao: O A2C é um método que combina elementos de atores e críticos. Ele é eficaz para tarefas contínuas e discretas e pode ser uma escolha sólida para jogos como Super Mario World.
tags:
  - tensorflow
---

