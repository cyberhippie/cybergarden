---
tipo:
  - estudo
title: Policy Gradient Methods
descricao: Algoritmos baseados em gradientes de políticas, como REINFORCE ou Proximal Policy Optimization (PPO), podem ser eficazes para aprender políticas que controlam o agente Mario. Eles direcionam diretamente a otimização da política.
tags:
  - tensorflow
---

