---
tipo:
  - estudo
title: Distributed Distributional Deterministic Policy Gradients (D4PG)
descricao: Este é um algoritmo mais avançado que lida com espaços de ação contínua e incerteza na distribuição de retornos. Pode ser uma opção se ações contínuas forem necessárias no jogo.
tags:
  - tensorflow
---

