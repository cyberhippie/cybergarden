---
tipo:
  - estudo
title: Deep Q-Network (DQN)
descricao: O DQN é uma extensão do Q-Learning que utiliza redes neurais profundas para aproximar a função Q. É amplamente usado em tarefas de aprendizado por reforço e pode ser aplicado com sucesso a jogos como o Super Mario World.
tags:
  - tensorflow
---

