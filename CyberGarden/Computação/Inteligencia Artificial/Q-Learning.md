---
tipo:
  - estudo
title: Q-Learning
descricao: O Q-Learning é um algoritmo de aprendizado por reforço tabular que é um bom ponto de partida para entender os conceitos fundamentais de RL. Você pode começar com ele para tarefas mais simples.
tags: 
---
# Resumo
O Q-learning é um dos algoritmos mais fundamentais de aprendizado por reforço e pertence à categoria de métodos de aprendizado por reforço baseados em valor. Foi desenvolvido por Christopher Watkins em 1989 e é usado para aprender uma política ótima para um agente em um ambiente com recompensas imediatas.

A ideia central por trás do Q-learning é aprender uma função de valor chamada de "função Q" (ou tabela Q) que avalia o valor de tomar uma determinada ação em um estado específico. A função Q é usada para determinar a melhor ação a ser tomada em um determinado estado para maximizar as recompensas cumulativas ao longo do tempo.

A função Q é atualizada iterativamente com base nas observações do [[Agente Inteligente|agente]]
interagindo com o ambiente. Aqui está um resumo do processo de treinamento do Q-learning:

1. **Inicialização da Tabela Q**: Inicialmente, a tabela Q é criada para mapear cada par (estado, ação) para um valor Q. Tipicamente, essa tabela é inicializada com valores arbitrários ou todos iguais a zero.

2. **Escolha da Ação**: O agente seleciona uma ação com base em uma política, que pode ser uma política ε-greedy, onde com probabilidade ε, o agente escolhe uma ação aleatória, e com probabilidade 1-ε, ele escolhe a ação com o maior valor Q no estado atual.

3. **Interagir com o Ambiente**: O agente realiza a ação escolhida no ambiente e observa o próximo estado e a recompensa imediata.

4. **Atualização da Tabela Q**: A tabela Q é atualizada com a fórmula de atualização do Q-learning, que é:

   Q(s, a) = Q(s, a) + α * [r + γ * max(Q(s', a')) - Q(s, a)]

   - Q(s, a) é o valor Q do estado atual (s) e da ação atual (a).
   - α (alfa) é a taxa de aprendizado que controla a rapidez com que a função Q é atualizada.
   - r é a recompensa imediata obtida.
   - γ (gama) é o fator de desconto que pondera a importância de recompensas futuras.
   - max(Q(s', a')) é o valor Q máximo do próximo estado (s') considerando todas as ações possíveis.

5. **Repetição**: O agente continua repetindo os passos 2-4, interagindo com o ambiente e atualizando a tabela Q até que a política converja para uma política ótima.

O Q-learning é conhecido por ser um método off-policy, o que significa que ele pode aprender a política ótima independentemente da política que o agente está seguindo durante o treinamento. Isso o torna um algoritmo muito versátil para uma ampla variedade de tarefas.

No entanto, o Q-learning tem suas limitações, principalmente em ambientes com espaços de estados e ações muito grandes, onde a tabela Q se torna impraticável. Para esses casos, variantes do Q-learning, como o Deep Q-Network ([[Deep Q-Network (DQN)|DQN]]), que usa redes neurais profundas para aproximar a função Q, são mais apropriadas. O [[Deep Q-Network (DQN)|DQN]] é uma extensão poderosa do Q-learning que permitiu a aplicação bem-sucedida do aprendizado por reforço em jogos complexos, como o Atari.
