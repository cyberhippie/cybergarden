---
tipo:
  - estudo
title: Asynchronous Advantage Actor-Critic (A3C)
descricao: O A3C é uma variação do A2C que é especialmente útil quando você deseja treinar seu agente em paralelo para acelerar o aprendizado.
tags:
  - tensorflow
---
