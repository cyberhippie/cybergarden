---
title: Altera e IA
tags: 
draft: true
---
A #Altera tem trabalhado em soluções para o desenvolvimento de IA. Uma delas, é uma ferramenta para o deploy de uma ecosistema em RISC-V para a execução de IA.

- Qual tipo de IA?
	- Deep learning?
	- LLM?

![[Pasted image 20241107130909.png]]
![[Pasted image 20241107131035.png]]
![[Pasted image 20241107132321.png]]

# Processador de propósito geral
![[Pasted image 20241107132354.png]]

[[uc-OS-II]]

[[Open On-Chip Debuger]]

# Desafios de integrar RISC-V em FPGA
![[Pasted image 20241107132909.png]]
![[Pasted image 20241107133427.png]]

