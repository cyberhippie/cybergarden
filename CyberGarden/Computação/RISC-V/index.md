---
tipo:
  - estudo
title: Introdução
descricao: "## Descrição"
tags:
  - RISC-V
consolidado: false
---
Então tá, você está embarcando comigo na jornada de se estudar sobre RISC-V. E aqui já começam os primeiros aprendizados. Logo de cara, fica complicado fizer que se trata de um estudo apenas da ISA (Instruction Set Computer), visto que RISC-V faz referência a:
- A instrução do processador em si;
- A comunidade de usuários de desenvolvedores da ISA;
- A organização RISC-V, que é responsável por manter a ISA e é detentora da propriedade intelectual dela;
- Os hardwares e produtos que são construídos sobre a ISA

# Um pouco de história - pra que não
A ideia de se construir um conjunto de instruções que fosse completamente aberta, surgiu da necessidade de se ter uma arquitetura, ou modelo de instruções, que pudesse ser utilizada por estudantes no processo de aprendizagem de [[Arquitetura de Computadores]].
## Origem do nome
RISC é um acrônimo para Reduced Instruction Set Computer, o que quer dizer que é uma das possíveis implementações de um modelo computacional, no qual o objetivo é manter um conjunto reduzido de instruções. Essa abordagem, já vinha sendo desenvolvida desde os 80s e já estava na sua quinta versão, daí que vêm o V no final.


