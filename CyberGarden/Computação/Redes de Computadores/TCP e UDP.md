São os dois principais protocolos da camada de transporte da internet. Sendo que o [[TCP e UDP#TCP|TCP]] é orientado a conexão e o [[TCP e UDP#UDP|UDP]] não é.
Em resumo, o UDP apenas empacota a mensagem e envia, permitindo que a própria aplicação crie seus protocolos sobre ele. Já o TCP, ele estabelece uma conexão, acrescenta confiabilidade com retransmissão, junto com controle de fluxo e controle de congestionamento.

# TCP

---

> [!tldr] TCP
> **Transmission Control Protocol**

# UDP

---

> [!tldr] UDP
> **User Datagram Protocol**

Uma aplicação que faz uso do UDP é o DNS, que responte apenas com o endereço de IP do host solicitado, não sendo necessário nenhuma configuração prévia para o envio da mensagem e nenhum encerramento. Bastando apenas enviar duas mensagens.

A vantagem dele é justamente o fato de ser rápido. A primeira vez que ouvi falar dele foi justamente no contexto de transmissão de uma vídeo chamada, onde ficar garantindo que todos os dados foram entregues não é tão necessário para o entendimento da conversa, e na verdade, talvez ficar verificando que todos os pacotes chegaram na ordem correta fosse gerar um congestionamento que ele sim iria atrapalhar o andamento da chamada.