---
tipo: estudo
title: Consistência e Replicação
descricao: |
 ## Descrição

tags: facul, SistemasDistribuídos
---
# Razões para replicar
Faz sentido começar pensando nos problemas de replicar e isso se resume em **consistência**. Sempre que tenho dois dados em lugares diferentes e algum desses dados é alterado de alguma forma, eu passo a ter problema de consistência. A correção disso se resume em repassar a alteração para as demais cópias.

Mas se replicar alguma informação pode me dar tanta dor de cabeça, por que fazer isso. A resposta se resume em escalabilidade, disponibilidade e resiliência de alguma informação ou recurso. Se você quer que algo seja acessado por diferentes pessoas de diferentes lugares e de maneira rápida, então você gostaria que esses recursos e dados estejam próximos a ela. Além disso, ter mais de uma cópia de uma informação me garante que essa informação não será perdida caso um dos portadores dela seja destruído.

## Operações conflitantes
- Read-Write
- Write-Write

# Consistência centrada nos dados
## Modelo do Sistema
Aqui ficou um pouco confuso, já que a definição que foi dada é bem semelhante a própria definição de um sistema distribuído. Nela, eu tenho uma coleção de unidades de armazenamento de informação as quais se comportam como uma única unidade.

## Sequencial - Lamport 1979
A ideia é que posso mover a ordem das mensagens (de leitura e escrita) de forma a garantir a consistência.
Então eu posso alterar a sequência temporal, a ordem com que as mensagens ocorrem, de maneira a criar uma história coerente.

## Causal 
Operações de escrita com alguma relação de causalidade devem ocorrer sempre em uma mesma sequência, isso quer dizer, independente do referencial, todo que tem a informação replicada, vai executar essa sequência de escritas na mesma ordem.

Mas não tem uma ordem total, quando não tem causalidade envolvida. Ex. Twitter.

## Read your Write - Terry 94
A ideia aqui é manter a consistência na sessão, ou seja, para um mesmo usuário. Então se estou editando uma página, eu certamente vou querer ver as minhas alterações. Neste caso, se eu faço um write eu tenho que ler necessariamente o que escrevi.

## Eventual - Vagels 2009
