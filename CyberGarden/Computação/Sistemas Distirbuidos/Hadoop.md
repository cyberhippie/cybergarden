---
tipo:
  - estudo
descricao: |
  Resuminho da aula de Sistemas Distribuídos referente a Hadoop
tags:
  - bigdata
---

# O que é Hadoop
- Framework open-source
- Desenvolvida pela Yahoo (copiando um projeto do Google)
- **Objetivo**: Armazenar e processar conjuntos massivos de dados 
- **Infraestrutura**: clusters de computadores
  > Feito para lidar com quantidades realmente massivas de dados (na casa do terabytes), por isso não é possível armazenar em um único computador. Daí que surge o #bigdata .
- Core
	- HDFS - Como sistema distribuído de arquivos
	- YARN - Como sistema de escalonamento de jobs
	- MapReduce - Como modelo de programação para o processamento massivo de dados

# Core
Máquinas separadas para o armazenamento das informações e para o processamento de dados.

## HDFS
- Sistema distribuído de arquivos
- Os servidores podem falhar, mas não abortar o processamento
- Os dados estão replicados no cluster

Se não posso melhorar a minha máquina, então aumento a quantidade de máquinas. Essa foi a ideia apresentada pelos fundadores do Google.

Tenho que considerar que agora vai ter falhas

Trabalhar com arquivos realmente gigantes

### Organização
Dividir o arquivo em Chunck, geralmente de 64MB (mas podendo ser também 128MB). Os Chunks são replicados (em maquinas, racks, datacenters)
Um nó master que armazena de cada arquivo onde está o seu Chunks.

### Arquiteturas
HDFS tem uma arquitetura master/slave, onde um master controla os slaves e não existe comunicação entre os slaves.
**Namenode** - Sabe onde estão todos os chunks.
	Poderia ser um ponto de gargalo, já que processa todas as informações de onde guardar cada arquivo. Porém a ideia dele é ser extremamente leve e rápido.
	
**Datanode** - Apenas armazena os chunks

## YARN: Yet Another Resource Negotiator
- Sistema de escalonamento de Jobs produzidos pelo processamento distribuído
### Organização
- Resource Manager - Controla o uso dos recursos, é ele que tem a visão geral do sistema como um todo.
- Node Manager - 
- MapReduce Application Master

## MapReduce Programing
- Modelo de programação que permite expressar o processamento distribuído em escala massiva.
Arquivozão -> Divide o arquivo em várias partes -> Executa o programas partes -> Junto os resultados.

## Princípios do Hadoop
- Localização de Dados
  Não mova os dados até os works, mova os works até os dados.
- Compartilhamento Zero
  Nenhuma task depende de outra
- Sincronização
  Processos concorrentes compartilham dados intermediários
- Tolerante à falhas
  Se tenho mil máquinas algumas vão morrer.
  Falhas são normais
  Se um worker cair, sua task deve ser executada novamente


# Sobre os dados
O armazenamento aumentou exponencialmente, porém a capacidade de acesso não. Qual é a solução distribuir a informação entre vários computadores.

# Futurologia
## Spark
Roda em cima do Hadoop
Executa várias rodadas de Map-Reduce
Não salva o resultado no HDFS durante as várias execuções do Map-Reduce