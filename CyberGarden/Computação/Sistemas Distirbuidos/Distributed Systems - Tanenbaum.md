---
annotation-target : Distributed_Systems_3-221027.pdf
---



>%%
>```annotation-json
>{"created":"2022-11-16T03:11:26.370Z","updated":"2022-11-16T03:11:26.370Z","document":{"title":"Distributed_Systems_3-221027.pdf","link":[{"href":"urn:x-pdf:fad2e4f7c723ace3c346bbc80695fc0b"},{"href":"vault:/Facul/Sistemas Distirbuidos/Distributed_Systems_3-221027.pdf"}],"documentFingerprint":"fad2e4f7c723ace3c346bbc80695fc0b"},"uri":"vault:/Facul/Sistemas Distirbuidos/Distributed_Systems_3-221027.pdf","target":[{"source":"vault:/Facul/Sistemas Distirbuidos/Distributed_Systems_3-221027.pdf","selector":[{"type":"TextPositionSelector","start":20825,"end":20948},{"type":"TextQuoteSelector","exact":"A distributed system is a collection of autonomous computing elementsthat appears to its users as a single coherent system.","prefix":"o give a loose characterization:","suffix":"This definition refers to two ch"}]}]}
>```
>%%
>*%%PREFIX%%o give a loose characterization:%%HIGHLIGHT%% ==A distributed system is a collection of autonomous computing elementsthat appears to its users as a single coherent system.== %%POSTFIX%%This definition refers to two ch*
>%%LINK%%[[#^j5n9hwlcm1d|show annotation]]
>%%COMMENT%%
>
>%%TAGS%%
>
^j5n9hwlcm1d
