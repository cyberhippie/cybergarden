---
tipo:
  - estudo
title: Definicao De Sistema Distribuido
descricao: |
  Anotações referentes ao primeiro capítulo do livro de Sistemas Distribuídos
tags:
---
![[Distributed Systems - Tanenbaum#^j5n9hwlcm1d]]
A proposta de um sistema distribuído é ter um conjunto de computadores, neste caso se tenho dois computadores já posso chamar de sistema distribuído, que se comporta para um cliente como um único computador.

Uma das características é justamente a coerência