---
tipo:
  - estudo
titulo: Ponteiros em C
descricao: |
  Este é um tópico bem interessante, já que revela bastante sobre como as informações estão armazenadas na memória do computador.
tags:
  - programação
  - c
---
# A Memória
Primeiramente, faz sentido ter uma visão mental de como é a memória RAM, aqui vamos falar apenas da memória RAM que é a única memória que realmente o nosso programa consegue ver. Tudo o que está no HD primeiro tem que ser carregado para a memória RAM para assim então o nosso programa conseguir trabalhar com a informação. Mas voltando a pensar na memória RAM, ela nada mais é do que uma fita gigante de bits e tudo o que o nosso programa faz é percorrer essa fita, realizando leitura e escrita nela.

![[Pasted image 20230222194558.png]]

> [!tip] Tá, mas se a fita é binária, como na imagem a cima, então como ela consegue guardar informações mais complexas como os números inteiros?
> A ideia é que a gente quebra essa fita em blocos, com um início e fim bem definidos e então utilizamos esses blocos para representar a informação.
> ## Bits e Bytes
> A menor porção de informação a gente chama de **bit**, é simplesmente um sim ou não. Porém, geralmente a gente agrupa os **bits** em grupos de 8 para formar um **byte**. Essa noção de que um byte são apenas 8 bits reunidos é muito importante para a computação.

## Variáveis
Quando a gente declara uma variável no nosso programa, essencialmente o que estamos fazendo é pedindo para o sistema operacional reservar um determinado espaço dessa fita gigante de memória para que possamos usa-lá. E o tamanho que a gente solicita ao sistema operacional depende do tipo de variável que estamos declarando. Mais pra frente em [[#Malloc]] vamos ver como nós mesmo podemos pedir um bloco de memória para o sistema operacional.

Porém, voltando as variáveis, a ideia de se ter tipos de variáveis, é justamente para especificar que cada tipo vai reservar uma quantidade diferente de espaço na fita e quando for ler a informação na fita, vai tratar os bits que lá estão de maneira diferente.

## Quero ver esse bagulho na prática!
Vamos começar brincando com valores inteiros, para isso vamos usar uma função chamada `sizeof()` que retorna o tamanho em bytes de uma variável.
```c
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv){
	int a, b;
	printf("%ld \n", sizeof(a));
	return 0;
}
```
A ideia com esse código é que estamos verificando qual o tamanho em bytes da variável `a`, que neste caso é 4. Repare que no `printf`foi utilizado `%ld`, isso indica que a variável que vamos imprimir é do tipo **long unsigned**. A tabela abaixo nos informa que um **long** tem 8 bytes, bem se quisermos verificar isso podemos simplesmente colocar um `sizeof()` dentro de outro e executar o código novamente.
![[Pasted image 20230222201532.png]]

### Tá, mas onde que entra ponteiro nisso?
Lembra que eu comentei que ponteiros são endereços da informação na memória, então se quisermos saber o endereço da informação basta perguntar ao programa, correto?

Para fazer isso em C e até em C++ a gente coloca um `&` na frente da variável. Repare que além de tirar o `sizeof()`, também mudei para `%p` a linha do `printf()`, isso porque, para imprimir um endereço é necessário especificar para o `printf` que se trata de um ponteiro.
```c
printf("%p \n", &a);
```
O resultado que você vai obter do código com as alterações acima vai ser uma coisa semelhante a isso: `0x7fff17923774`. E é isso, endereços de memória são representados com esses números em hexadecimal (por isso um `0x` na frente do número) feios mesmo.

### Como ver que são realmente endereços?
Aqui começa uma brincadeira interessante que dá para fazer em C. Como é tudo endereço, eu posso dar uma bisbilhotada no coleguinha caso tenha o seu endereço.

Reparou que no código anterior tinha um
```c
int a, b;
```
Quando duas variáveis são declaradas logo em seguida, as changes são bem grandes de que elas estejam uma ao lado da outra na memória. Bem, podemos verificar isso de uma maneira bem simples:
```c
printf("%p \n", &a);
printf("%p \n", &b);
```
A minha saída foi como esperado, uma diferença de 4 bytes entre os dois endereços.
```sh
0x7ffe0a5deb50 
0x7ffe0a5deb54 
```
O que significa que se você pegar o endereço de `a` e somar a ele 4 bytes, você chega em `b`.
```c
printf("%d \n", *(&a+1));
```
Agora você deve estar se perguntando que bruxaria foi aquela ao redor do pobre `a` ! Pensa o seguinte, se a gente tem o `&` para ler o endereço de uma variável, tem que existir algo para, dado um endereço conseguir ler o valor que tem naquele endereço, e é para isso que utilizamos o `*` . Então o passo a passo do que aconteceu com o `a` ali, primeiro nós pegamos o endereço de `a` e então realizamos uma operação de soma com ponteiros. Como é uma soma de ponteiro, somar 1 significa acrescentar mais um no tamanho daquele dado. Tá, imagino que deva ter ficado um pouco confuso, mas imagina assim: um `int` tem 4 bytes, então o esperado era somar +4 no endereço, porém o compilador já sabe que o dado é um `int` então o +1 significa que você está acrescentando mais uma unidade do tamanho de `int`. E depois de tudo isso, eu pego aquela porção nova de memória que o meu endereço está apontando e resolvo ela com o `*` , então agora a minha informação (que estou trabalhando) volta a ser um inteiro, para que assim enfim, o dado seja impresso na tela.

# Malloc
Até este ponto, simplesmente aceitamos que quando criamos uma variável, simplesmente o programa já vai pedir um espaço de memória para o sistema operacional e vamos estar livres para usar esse espaço. Mas e se nós quisermos fazer isso por conta própria? Esse tipo de situação geralmente acontece quando:
- Não temos ideia de quantas variáveis teremos que gerar para fazer uma determinada tarefa do programa;
- Quando queremos um tamanho de memória muito específico, tipo quando usamos [[struct]];
- Quando queremos ter total controle de quando aquele bloco de memória será devolvido para o sistema operacional.


# Ponteiro como atributo de função

# O outro lado de vetores e matrizes