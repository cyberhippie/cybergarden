---
title: Principia
---
![[principia-emicida.png]]
# Bem vinde!!

Este é o meu pequeno espaço na internet.

Meu plano por aqui é ir deixando minhas anotações, sobre tudo aquilo que venho aprendendo, acessível para todos os falantes de português. Digo, ao menos será nesse idioma que eu vou codificar as minhas ideias, já que as ideias em sí, as vezes são confusas de mais para entender. Quase sempre, nem eu mesmo me entendo.